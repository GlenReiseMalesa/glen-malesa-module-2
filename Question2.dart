void main() {
  var apps = {
    '2012': 'FNB Banking app',
    '2013': 'SnapScan',
    '2014': 'LIVE Inspect',
    '2015': 'WumDrop',
    '2016': 'Domestly',
    '2017': 'Standard Banks Shyft',
    '2018': 'Khula',
    '2019': 'Naked Insurance',
    '2020': 'EasyEquities',
    '2021': 'Ambani Africa'
  };

  var sortMapByValue = Map.fromEntries(
      apps.entries.toList()..sort((e1, e2) => e1.value.compareTo(e2.value)));
  print("**Sorted Apps**");
  print(sortMapByValue.values);

  var winner2017 = apps['2017'];
  print("winning app of 2017 : $winner2017");

  var winner2018 = apps['2018'];
  print("winning app of 2018 : $winner2018");

  var size = apps.length;
  print("total number of apps : $size");
}
